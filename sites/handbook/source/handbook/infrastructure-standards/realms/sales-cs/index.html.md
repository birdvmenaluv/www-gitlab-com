---
layout: handbook-page-toc
title: "Sales Customer Success Realm"
description: "This handbook section defines the latest iteration of infrastructure standards for AWS and GCP across all departments and groups at GitLab."
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Quick links

* [Global infrastructure standards](/handbook/infrastructure-standards/)
* [Global labels and tags](/handbook/infrastructure-standards/labels-tags/)
* [Infrastructure policies](/handbook/infrastructure-standards/policies/)
* [Infrastructure helpdesk](/handbook/infrastructure-standards/helpdesk/)

## Overview

This realm is for the Customer Success team to deploy shared and team-specific infrastructure resources.

### Access requests

To request access to a group, please see [group access request tutorial](/handbook/infrastructure-standards/tutorials/groups/access-request/).

> For email authenticity security reasons, only GitLab issues or Slack messages to owners or counterparts are allowed for infrastructure requests.

### Realm Owners

| Name                 | GitLab.com Handle       | Role        | Job Title                                             |
|----------------------|-------------------------|-------------|-------------------------------------------------------|
| Jeff Martin          | `jeffersonmartin`       | Owner       | Senior Demo Systems Engineer                          |
| Cristiano Casella    | `ccasella`              | Counterpart | Technical Account Manager (Demo Admins working group) |
| James Sandlin        | `jsandlin`              | Counterpart | Solutions Architect (Demo Admins working group)       |

## Realm labels and tags

The [global labels/tags](/handbook/infrastructure-standards/labels-tags) and [realm labels/tags](/handbook/infrastructure-standards/realms/sales-cs/labels-tags) should be applied to each resource.

## Realm Groups

Each infrastructure group has a shared GCP project and/or AWS account for group members.

If a group has not been implemented yet, please contact the realm owner for assistance. After a group is implemented, a separate handbook page is created with usage documentation.

| Group Name (AWS Account/GCP Project Name) | Usage Documentation (Empty cells are not implemented yet)                                                                       |
|-------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------|
| `sales-channel-shared-infra`              | <!--[Group Docs](/handbook/infrastructure-standards/realms/sales-cs/groups/sales-channel-shared-infra)-->                       |
| `sales-cs-shared-infra`                   | <!--[Group Docs](/handbook/infrastructure-standards/realms/sales-cs/groups/sales-cs-shared-infra)-->                            |
| `sales-cs-demo-cloud`                     | <!--[Group Docs](/handbook/infrastructure-standards/realms/sales-cs/groups/sales-cs-demo-cloud)-->                              |
| `sales-cs-training-cloud`                 | <!--[Group Docs](/handbook/infrastructure-standards/realms/sales-cs/groups/sales-cs-training-cloud)-->                          |
| `sales-cs-sa-shared-infra`                | <!--[Group Docs](/handbook/infrastructure-standards/realms/sales-cs/groups/sales-cs-sa-shared-infra)-->                         |
| `sales-cs-sa-us-west`                     | <!--[Group Docs](/handbook/infrastructure-standards/realms/sales-cs/groups/sales-cs-sa-us-west)-->                              |
| `sales-cs-sa-us-east`                     | <!--[Group Docs](/handbook/infrastructure-standards/realms/sales-cs/groups/sales-cs-sa-us-east)-->                              |
| `sales-cs-sa-pub-sec`                     | <!--[Group Docs](/handbook/infrastructure-standards/realms/sales-cs/groups/sales-cs-sa-pub-sec)-->                              |
| `sales-cs-sa-mid-market`                  | <!--[Group Docs](/handbook/infrastructure-standards/realms/sales-cs/groups/sales-cs-sa-mid-market)-->                           |
| `sales-cs-sa-emea`                        | <!--[Group Docs](/handbook/infrastructure-standards/realms/sales-cs/groups/sales-cs-sa-emea)-->                                 |
| `sales-cs-sa-apac`                        | <!--[Group Docs](/handbook/infrastructure-standards/realms/sales-cs/groups/sales-cs-sa-apac)-->                                 |
| `sales-cs-tam-shared-infra`               | <!--[Group Docs](/handbook/infrastructure-standards/realms/sales-cs/groups/sales-cs-tam-shared-infra)-->                        |
| `sales-cs-tam-us-west`                    | <!--[Group Docs](/handbook/infrastructure-standards/realms/sales-cs/groups/sales-cs-tam-us-west)-->                             |
| `sales-cs-tam-us-east`                    | <!--[Group Docs](/handbook/infrastructure-standards/realms/sales-cs/groups/sales-cs-tam-us-east)-->                             |
| `sales-cs-tam-pub-sec`                    | <!--[Group Docs](/handbook/infrastructure-standards/realms/sales-cs/groups/sales-cs-tam-pub-sec)-->                             |
| `sales-cs-tam-emea`                       | <!--[Group Docs](/handbook/infrastructure-standards/realms/sales-cs/groups/sales-cs-tam-emea)-->                                |
| `sales-cs-tam-apac`                       | <!--[Group Docs](/handbook/infrastructure-standards/realms/sales-cs/groups/sales-cs-tam-apac)-->                                |
| `sales-cs-ps-shared-infra`                | <!--[Group Docs](/handbook/infrastructure-standards/realms/sales-cs/groups/sales-cs-ps-shared-infra)-->                         |
| `sales-cs-ps-consulting`                  | <!--[Group Docs](/handbook/infrastructure-standards/realms/sales-cs/groups/sales-cs-ps-consulting)-->                           |
| `sales-cs-ps-education`                   | <!--[Group Docs](/handbook/infrastructure-standards/realms/sales-cs/groups/sales-cs-ps-education)-->                            |
| `sales-cs-ps-pub-sec`                     | <!--[Group Docs](/handbook/infrastructure-standards/realms/sales-cs/groups/sales-cs-ps-pub-sec)-->                              |

## Usage guidelines

This is a placeholder for the realm owner to provide instructions on best practices and usage guidelines for this infrastructure.
